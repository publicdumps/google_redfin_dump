## redfin-user 12 SQ1A.211205.008 7888514 release-keys
- Manufacturer: google
- Platform: lito
- Codename: redfin
- Brand: google
- Flavor: redfin-user
- Release Version: 12
- Id: SQ1A.211205.008
- Incremental: 7888514
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/redfin/redfin:12/SQ1A.211205.008/7888514:user/release-keys
- OTA version: 
- Branch: redfin-user-12-SQ1A.211205.008-7888514-release-keys
- Repo: google_redfin_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
